package com.susnservice.susnservice.Controller;



import com.susnservice.susnservice.Model.SignUpResponse;
import com.susnservice.susnservice.Model.SignUpResquest;
import com.susnservice.susnservice.Service.SusnService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1/susn")
@Slf4j
public class SusnController {

    @Autowired
    SusnService susnService;

    @GetMapping()
    public String statusOfService(){
        return "working";
    }

    @PostMapping(path = "/signUp")
    public ResponseEntity<SignUpResponse> signUp(@RequestHeader String txRoot,
                                 @RequestHeader String SystemId,
                                 @RequestBody SignUpResquest signUpResquest){
        log.info(" Data : txRoot : {}, SystemId : {}, Body : {}", txRoot, SystemId, signUpResquest);
        SignUpResponse signUpResponse = susnService.userSignUp(txRoot, SystemId, signUpResquest);
        return new ResponseEntity<>(signUpResponse, HttpStatus.CREATED);

    }
}

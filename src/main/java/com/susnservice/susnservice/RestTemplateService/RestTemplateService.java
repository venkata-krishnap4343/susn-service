package com.susnservice.susnservice.RestTemplateService;

import com.susnservice.susnservice.Model.SignUpResponse;
import com.susnservice.susnservice.Model.SignUpResquest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class RestTemplateService {

    @Autowired
    RestTemplate restTemplate;

    @Value("${database-service.micro-service.signUp}")
    private String signUpUrl;

    public ResponseEntity<SignUpResponse> signUp(String txRoot, String SystemId, SignUpResquest signUpResquest){
        final String uri = signUpUrl;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("txRoot",  txRoot);
        headers.add("SystemId", SystemId);
        HttpEntity<SignUpResquest> entity = new HttpEntity<SignUpResquest>(signUpResquest, headers);
        log.info("Request Data: TxRoot :{}, SystemId :{}",txRoot, SystemId);
        ResponseEntity<SignUpResponse> result =  restTemplate.exchange(uri, HttpMethod.POST, entity, SignUpResponse.class);
        log.info(" Response log : user Id: {}, email: {} ", result.getBody().getUserId(), result.getBody().getEmail());
        return result;
    }
}
